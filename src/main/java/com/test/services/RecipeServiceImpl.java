package com.test.services;


import com.test.model.Recipe;

import com.test.repository.RecipeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

@Service
@Slf4j
public class RecipeServiceImpl implements RecipeService {

    @Autowired
    RecipeRepository repo;


    @Override
    public Recipe createRecipe(Recipe r) throws Exception {
        if (r.getId() == null) {
            log.info("Adding Recipe to database");
            return repo.save(r);

        } else {
            throw new Exception("cannot add recipe");
        }
    }

    @Override
    public Iterable<Recipe> getRecipes() throws Exception {
        if (repo.count() > 0) {
            log.info("Getting all recipes");
            return repo.findAll();
        } else {
            throw new Exception("There are no recipes");
        }
    }

    @Override
    public String deleteRecipe(int id) throws Exception {
        if(repo.existsById(id)) {
            log.info("Deleting Recipe id: {}", id);
            repo.deleteById(id);
            return "Recipe deleted";
        } else {
            throw new Exception("Recipe doesn't Exist");
        }
    }
}