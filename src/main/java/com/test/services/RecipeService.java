package com.test.services;


import com.test.model.Recipe;
import org.springframework.http.HttpStatus;


public interface RecipeService {
    public Recipe createRecipe(Recipe r) throws Exception;
    public String deleteRecipe(int id) throws Exception;
    public Iterable<Recipe> getRecipes() throws Exception;
}
