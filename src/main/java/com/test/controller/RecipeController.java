package com.test.controller;

import com.test.model.Recipe;
import com.test.services.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;


// annotation for rest com.test.controller
@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping(path = "/recipe")
public class RecipeController {

    @Autowired
    RecipeService recipeService;

    @GetMapping("")
    public ResponseEntity<Iterable<Recipe>> getRecipes() {
        try {
            return new ResponseEntity<Iterable<Recipe>>(recipeService.getRecipes(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("")
    public ResponseEntity<Recipe> createRecipe(@RequestBody @Valid Recipe r) {
        try {
            return new ResponseEntity<Recipe>(recipeService.createRecipe(r), HttpStatus.OK);
        } catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping("{id}")
    public ResponseEntity<String> deleteRecipe(@PathVariable int id) {
        try {
            return new ResponseEntity<String>(recipeService.deleteRecipe(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
