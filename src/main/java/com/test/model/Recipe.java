package com.test.model;


import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotNull;



@Data
@Entity
@Table(name = "recipe")
public class Recipe {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer Id;

    @Column(name = "name")
    @NotNull
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "instructions", length = 10000)
    private String instructions;

    @Column(name = "ingredients", length = 10000)
    private String ingredients;

}

